# *The Intercept* with Inform 7 and Vorple #

This is an Inform 7 port of *The Intercept*, a game written by inkle. It uses [inkjs](https://github.com/y-lohse/inkjs) and [Vorple](http://vorple-if.com/) to allow Inform to use ink directly.

It is a fairly straightforward port, so it does not show the potential of an Inform 7 project using ink. It shows however how one would present an ink story and its choices with Vorple.

## Try the game ##

You can try the game [here](http://ulukos.com/play/the-intercept/).

## Use ink with Vorple ##

The "Vorple ink" extension, allowing an Inform 7 game, to use ink is available [here](https://bitbucket.org/Natrium729/extensions-inform-7/). Its documentation, along with [ink's tutorial](https://github.com/inkle/ink#ink), will get you started.