"The Intercept" by inkle

The story genre is "Mystery".
The story description is "Bletchley Park, 1942. A component from the Bombe machine, used to decode intercepted German messages, has gone missing. One of the cryptographers is waiting to be interviewed, under direst suspicion. Is he stupid enough to have attempted treason? Or is he clever enough to get away?

(This is a Inform 7 port of the Unity game, using Vorple.)".
The story creation year is 2016.

Include Vorple Screen Effects by Juhana Leinonen.
Include Vorple Command Prompt Control by Juhana Leinonen.
Include Vorple Hyperlinks by Juhana Leinonen.
Include Vorple Element Manipulation by Juhana Leinonen.
Include Vorple ink by Nathanael Marion.

Release along with the "Vorple" interpreter and the cover art.
Release along with JavaScript "ink.js".
Release along with the file "story.json".

[We stop the game if it is not played in Vorple.]
First when play begins:
	if Vorple is not supported:
		say "Sorry, this story needs the Vorple interpreter.";
		wait for any key;
		stop the game abruptly.

[The mandatory room in an Inform Project. It is not used in this one.]
There is a room.

[So that the name of the room is not displayed after the banner.]
The initial room description rule is not listed in the startup rulebook.

[Removes the markup in the text.
TODO: Doesn't work. The tags are removed, but the text is not displayed in italic.]
To decide what text is the sanitised form of (T - a text):
	replace the text "<i>" in T with "[italic type]";
	replace the text "</i>" in T with "[roman type]";
	decide on T.

[The choices will be made with hyperlinks, so we don't need the prompt.]
When play begins:
	hide the prompt.

[To start the ink story at the beginning of the game.]
The ink initialisation command is "play ink".

[This action displays the lines from ink and the choices.]
Playing ink is an action out of world applying to nothing.
Understand "play ink" as playing ink.

Carry out playing ink:
	scroll to the end of the page;
	display the ink story;
	if the current number of ink choices is greater than 0:
		display the ink choices;
	else: [There are no choices left.]
		end the story.

[Displays the lines of the ink story one by one.]
To display the ink story:
	while ink can continue:
		say "[sanitised form of the next ink line]";
		scroll to the end of the page;
		wait for any key.

[Display the ink choices in an HTML ordered list.]
To display the ink choices:
	open HTML tag "ol" called "choices"; [The list.]
	let L be the current ink choices;
	repeat with X running from 1 to the number of entries in L:
		open HTML tag "li"; [An element of the list.]
		let C be entry X of L; [The text of the choice.]
		now C is the sanitised form of C;
		place a link to command "choose [X]" reading "[C]", without showing the command;
		close HTML tag; [/li]
	close HTML tag; [/ol]
	scroll to the end of the page.

[This action is for choosing an option.]
Choosing is an action out of world applying to a number.
Understand "choose [number]" as choosing.

Carry out choosing:
	remove the element called "choices"; [removes the list from the page.]
	select ink choice number (number understood);
	queue the parser command "play ink", without showing the command; [And we go for another round!]